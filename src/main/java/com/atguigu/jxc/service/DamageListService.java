package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;
import java.util.Map;

public interface DamageListService {


    ServiceVO saveDamageListGoods(String damageNumber, DamageList damageList, List<DamageListGoods> damageListGoods);


    Map<String, Object> getList(String sTime, String eTime);

    Map<String, Object> getGoodsList(Integer damageListId);

}
