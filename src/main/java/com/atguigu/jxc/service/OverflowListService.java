package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;
import java.util.Map;

public interface OverflowListService {
    ServiceVO saveOverflowListGoods(List<OverflowListGoods> overflowListGoods, OverflowList overflowList, String overflowNumber);

    Map<String, Object> getOverflowList(String sTime, String eTime);

    Map<String, Object> getOverflowGoodsList(Integer overflowListId);
}
