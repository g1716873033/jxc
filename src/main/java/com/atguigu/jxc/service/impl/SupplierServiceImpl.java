package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;
    @Autowired
    private LogService logService;
    @Override
    public Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {
        Map<String,Object>map=new HashMap<>();
        int total = supplierDao.getSupplierPage(supplierName);
        page=page==0?1 : page;
        int offSet = (page - 1) * rows;
        List<Supplier>supplierList= supplierDao.getSupplierList(offSet,rows,supplierName);
        logService.save(new Log(Log.SELECT_ACTION, "分页查询供应商信息"));
        map.put("rows",supplierList);
        map.put("total",total);

        return map;
    }

    @Override
    public ServiceVO save(Supplier supplier) {
        if(supplier.getSupplierId()==null){
            Supplier supplier1=supplierDao.findSupplierByName(supplier.getSupplierName());

            if(supplier1!=null){
                return new ServiceVO<>(ErrorCode.ROLE_EXIST_CODE, ErrorCode.ROLE_EXIST_MESS);
            }
            logService.save(new Log(Log.INSERT_ACTION, "新增角色:" + supplier.getSupplierName()));

            supplierDao.insertSupplier(supplier);
        }else {
            logService.save(new Log(Log.UPDATE_ACTION, "修改角色:"+supplier.getSupplierName()));

            supplierDao.updateSupplier(supplier);
        }


        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(String ids) {

        logService.save(new Log(Log.DELETE_ACTION,"删除供应商:"+supplierDao.getSupplierById(ids).getSupplierName()));

        supplierDao.deleteSupplier(ids);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


}

