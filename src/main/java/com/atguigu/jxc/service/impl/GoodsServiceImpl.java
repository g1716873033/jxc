package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private LogService logService;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> getStockGoods(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
         Map<String,Object>map=new HashMap<>();
        int total = goodsDao.getStockCount(codeOrName,goodsTypeId);
         page=page==0?1 : page;
        int offSet = (page - 1) * rows;
         List<Goods>goodsList = goodsDao.getStockGoods(offSet,rows,codeOrName,goodsTypeId);
        logService.save(new Log(Log.SELECT_ACTION, "分页查询商品信息"));
        map.put("rows",goodsList);
        map.put("total",total);

        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String,Object>map = new HashMap<>();
        int total= goodsDao.getStockCount(goodsName,goodsTypeId);
        page=page==0?1 :page;
        int offSet = (page-1)*rows;
        List<Goods>goodsList = goodsDao.getGoodsList(offSet,rows,goodsName,goodsTypeId);
        logService.save(new Log(Log.SELECT_ACTION,"分页查商品"));
        map.put("rows",goodsList);
        map.put("total",total);
        return map;
    }

    @Override
    public ServiceVO saveGoods(Goods goods) {
        if(goods.getGoodsId()==null){
            logService.save(new Log(Log.INSERT_ACTION,"新增商品"+goods.getGoodsName()));
            goodsDao.insertGoods(goods);
        }else {
            logService.save(new Log(Log.UPDATE_ACTION,"修改商品"+goods.getGoodsName()));
            goodsDao.updateGoods(goods);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteGoods(Integer goodsId) {
        Goods goods = goodsDao.getGoodById(goodsId);
        if(goods.getState()==0){
            logService.save(new Log(Log.DELETE_ACTION,"删除商品"+goods.getGoodsName()));
            goodsDao.deleteGoods(goodsId);
        }else {return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);}
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object>map = new HashMap<>();
        int total = goodsDao.getNoInventoryStock(nameOrCode);
        page=page==0?1 : page;
        int offSet = (page - 1) * rows;
        List<Goods>goodsList = goodsDao.getNoInventGoodsList(offSet,rows,nameOrCode);
        logService.save(new Log(Log.SELECT_ACTION,"分页查无库存商品"));
        map.put("rows",goodsList);
        map.put("total",total);
        return map;

    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object>map = new HashMap<>();
        int total = goodsDao.getHasInventoryStock(nameOrCode);
        page=page==0?1 : page;
        int offSet = (page - 1) * rows;
        List<Goods>goodsList = goodsDao.getHasInventGoodsList(offSet,rows,nameOrCode);
        logService.save(new Log(Log.SELECT_ACTION,"分页查有库存商品"));
        map.put("rows",goodsList);
        map.put("total",total);
        return map;
    }

    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        logService.save(new Log(Log.UPDATE_ACTION,"修改商品"+goodsId));
        goodsDao.updateGoodsStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goodById = goodsDao.getGoodById(goodsId);
        if(goodById.getState()==0){logService.save(new Log(Log.DELETE_ACTION,"删除商品"+goodById.getGoodsName()));
            goodsDao.updateGoodsStocknew(goodsId,goodById.getInventoryQuantity(),goodById.getPurchasingPrice());
        }else {return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);}
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

        }

    @Override
    public Map<String, Object> getListAlarm() {
        Map<String,Object>map = new HashMap<>();
        List<Goods> goodsList=goodsDao.getListAlarm();
        map.put("rows",goodsList);
        return map;

    }


}
