package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private LogService logService;
    @Override
    public Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName) {

        Map<String,Object>map=new HashMap<>();
        int total = customerDao.getCustomerPage(customerName);
        page=page==0?1 : page;
        int offSet = (page - 1) * rows;
        List<Customer> customerList= customerDao.getCustomerList(offSet,rows,customerName);
        logService.save(new Log(Log.SELECT_ACTION, "分页查询客户信息"));
        map.put("total",total);
        map.put("rows",customerList);


        return map;
    }

    @Override
    public ServiceVO save(Customer customer) {
        if(customer.getCustomerId()==null){
//           Customer customer1= customerDao.findCustomerByName(customer.getCustomerName());
//           if(customer1!=null){
//               return new ServiceVO<>(ErrorCode.ROLE_EXIST_CODE ,ErrorCode.ROLE_EXIST_MESS);
//           }
            logService.save(new Log(Log.INSERT_ACTION, "新增角色:" + customer.getCustomerName()));

            customerDao.insertCustomer(customer);
        }else {
            logService.save(new Log(Log.UPDATE_ACTION, "修改角色:"+customer.getCustomerName()));

            customerDao.updateCustomer(customer);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(String ids) {
logService.save(new Log(Log.DELETE_ACTION,"删除客户"+customerDao.getCustomerById(ids).getCustomerName()));
customerDao.delete(ids);
return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
