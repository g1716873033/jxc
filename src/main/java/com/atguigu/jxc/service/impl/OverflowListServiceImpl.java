package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListServiceImpl implements OverflowListService {
    @Autowired
    private OverflowListDao overflowListDao;
    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;
    @Override
    public ServiceVO saveOverflowListGoods(List<OverflowListGoods> overflowListGoods, OverflowList overflowList, String overflowNumber) {
       overflowList.setUserId(1);
       overflowListDao.insertOverflowList(overflowList);
        overflowListGoods.forEach(overflowListGoods1 -> {
        overflowListGoods1.setOverflowListGoodsId(overflowList.getOverflowListId());
        });
        overflowListGoodsDao.insertOverflowListGoods(overflowListGoods);


        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        Map<String,Object>map=new HashMap<>();
        List<OverflowList>overflowLists=overflowListDao.getOverflowList(sTime,eTime);
        map.put("rows",overflowLists);
        return map;
    }

    @Override
    public Map<String, Object> getOverflowGoodsList(Integer overflowListId) {
        Map<String,Object>map=new HashMap<>();
        List<OverflowListGoods>overflowListGoods=overflowListGoodsDao.getOverflowGoodsList(overflowListId);
        map.put("rows",overflowListGoods);

        return map;
    }
}
