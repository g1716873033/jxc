package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListServiceImpl implements DamageListService {
    @Autowired
    private DamageListDao damageListDao;
    @Autowired
    private DamageListGoodsDao damageListGoodsDao;


    @Override

    public ServiceVO saveDamageListGoods(String damageNumber, DamageList damageList, List<DamageListGoods> damageListGoods) {
        damageList.setUserId(1);
        damageListDao.insertDamageList(damageList);
        damageListGoods.forEach(damageListGoods1 -> {
            damageListGoods1.setDamageListId(damageList.getDamageListId());
        });

        damageListGoodsDao.insertDamageListGoods(damageListGoods);


        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getList(String sTime, String eTime) {
        Map<String,Object>map=new HashMap<>();
        List<DamageList> damageLists = damageListDao.getList(sTime,eTime);
        map.put("rows",damageLists);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer damageListId) {
        Map<String,Object>map=new HashMap<>();
        List<DamageListGoods>damageListGoods=damageListGoodsDao.getGoodsList(damageListId);
        map.put("rows",damageListGoods);
        return map;
    }


}

