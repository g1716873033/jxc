package com.atguigu.jxc.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {
    @Autowired
    private DamageListService damageListService;

   @PostMapping("save")
    public ServiceVO saveDamageList(String damageNumber,
                                    DamageList damageList,
                                    String damageListGoodsStr){

       List<DamageListGoods> damageListGoods = JSON.parseArray(damageListGoodsStr, DamageListGoods.class);
      return damageListService.saveDamageListGoods(damageNumber,damageList,damageListGoods);
   }

   @PostMapping("/list")
    public Map<String,Object> getList(String sTime,String  eTime){
       return damageListService.getList(sTime,eTime);
   }
   @PostMapping("/goodsList")
    public Map<String,Object>getGoodsList(Integer damageListId){
       return damageListService.getGoodsList(damageListId);

   }
}
