package com.atguigu.jxc.controller;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListController {
    @Autowired
    private OverflowListService overflowListService;
    @PostMapping("/save")
    public ServiceVO saveOverflowListGoods(String overflowNumber,OverflowList overflowList, String overflowListGoodsStr){
        List<OverflowListGoods> overflowListGoods = JSON.parseArray(overflowListGoodsStr, OverflowListGoods.class);
      return overflowListService.saveOverflowListGoods(overflowListGoods,overflowList,overflowNumber);
    }

    @PostMapping("/list")
    public Map<String,Object>getOverflowList(String  sTime, String  eTime){
        return overflowListService.getOverflowList(sTime,eTime);

    }
    @PostMapping("/goodsList")
    public Map<String,Object>getOverflowGoodsList(Integer overflowListId){
        return overflowListService.getOverflowGoodsList(overflowListId);
    }
}
