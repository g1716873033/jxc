package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    int getCustomerPage(@Param("customerName") String customerName);

    List<Customer> getCustomerList(@Param("offSet") Integer offSet, @Param("pageRow") Integer pageRow, @Param("customerName") String customerName);

    Customer findCustomerByName( @Param("customerName") String customerName);

    void insertCustomer(Customer customer);

    void updateCustomer(Customer customer);

    Customer getCustomerById(@Param("ids") String ids);

    void delete(@Param("ids") String ids);
}
