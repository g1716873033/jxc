package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */

public interface GoodsDao {


    String getMaxCode();


    List<Goods> getStockGoods(@Param("offSet") Integer offSet,@Param("pageRow") Integer pageRow, String codeOrName, Integer goodsTypeId);

    Integer getStockCount(String codeOrName, Integer goodsTypeId);

    List<Goods> getGoodsList( @Param("offSet") Integer offSet,@Param("pageRow") Integer rows,@Param("goodsName") String goodsName,@Param("goodsTypeId") Integer goodsTypeId);

    void insertGoods(Goods goods);

    void updateGoods(Goods goods);

    Goods getGoodById(@Param("goodsId") Integer goodsId);

    void deleteGoods(@Param("goodsId") Integer goodsId);

    int getNoInventoryStock(@Param("nameOrCode") String nameOrCode);

    List<Goods> getNoInventGoodsList(@Param("offSet") int offSet,@Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    int getHasInventoryStock(@Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventGoodsList(@Param("offSet") int offSet,@Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);
    void updateGoodsStock(@Param("goodsId")Integer goodsId, @Param("inventoryQuantity")Integer inventoryQuantity,@Param("purchasingPrice") double purchasingPrice);



    void updateGoodsStocknew(@Param("goodsId")Integer goodsId,  @Param("inventoryQuantity")Integer inventoryQuantity, double purchasingPrice);

    List<Goods> getListAlarm();

}
