package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListGoodsDao {
    void insertOverflowListGoods(List<OverflowListGoods> overflowListGoods);

    List<OverflowListGoods> getOverflowGoodsList(@Param("overflowListId") Integer overflowListId);
}



