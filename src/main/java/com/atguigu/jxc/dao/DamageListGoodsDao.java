package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListGoodsDao {
    void insertDamageListGoods( List<DamageListGoods> damageListGoods);

    List<DamageListGoods> getGoodsList(@Param("damageListId") Integer damageListId);
}
