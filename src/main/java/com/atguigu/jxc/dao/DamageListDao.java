package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListDao {

    void insertDamageList(DamageList damageList);

    List<DamageList> getList(@Param("sTime") String sTime,@Param("eTime") String eTime);
}
