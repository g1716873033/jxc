package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    GoodsType getGoodsTypeById(@Param("goodsTypeId") Integer goodsTypeId) ;


    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);




    GoodsType getGoodsTypeByName(@Param("goodsTypeName") String goodsTypeName);

    void insertGoodsType(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId);


    void deleteGoodsType(@Param("goodsTypeId") Integer goodsTypeId);

    int count(@Param("pId") Integer pId);

    void updateGoodsTypeState(GoodsType goodsType);

    void updateGoodsTypePid(@Param("goodsTypeId") Integer goodsTypeId);
}
