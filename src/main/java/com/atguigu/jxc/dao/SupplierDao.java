package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;


import java.util.List;


public interface SupplierDao {
    int getSupplierPage(@Param("supplierName") String supplierName);


    List<Supplier> getSupplierList(@Param("offSet") Integer offSet, @Param("pageRow") Integer pageRow, @Param("supplierName") String supplierName);

    Supplier findSupplierByName(@Param("supplierName") String supplierName);

    void insertSupplier(Supplier supplier);

    void updateSupplier(Supplier supplier);

   Supplier getSupplierById(@Param("ids") String ids);

    void deleteSupplier(@Param("ids") String ids);
}
